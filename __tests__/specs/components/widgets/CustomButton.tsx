
import React from 'react';
import {TouchableOpacity} from 'react-native';
import renderer from 'react-test-renderer';
import {shallow, ShallowWrapper} from 'enzyme';

import {CustomButton} from '../../../../src/components/widgets';



describe('CustomButton', () => {

    let wrapper: ShallowWrapper;
    const props = {
        title: 'Foo',
        onPress: jest.fn()
    }

    beforeEach(() => {
        wrapper = shallow(<CustomButton {...props} />);
    });

    test('Render', () => {
        renderer.create(<CustomButton title="Test" onPress={props.onPress} />);
    });

    test('Checking title prop', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Calling onPress prop', () => {

        wrapper.find(TouchableOpacity).simulate('press', props.onPress);

        expect(props.onPress).toHaveBeenCalledTimes(1);
    });
});

