
// It's a dummy test

describe('Utils - String', () => {

    test('Check length', () => {

        const foo = 'Hola';

        expect(foo.length).toBe(4);
    });
});
