
import React, {useState} from 'react';
import {SafeAreaView, View, Text, Button, StyleSheet} from 'react-native';
import {CustomButton} from './components/widgets';


export const App = () => {
	
	const [clickAmount, setClickAmount] = useState<number>(0);

	const onButtonClick = (): void => {
		setClickAmount(clickAmount + 1);
	}

	return (
		<SafeAreaView style={styles.container} >

			<View>
				<Text style={styles.title} >
					Clicked {clickAmount} times
				</Text>
			</View>

			<View style={styles.buttonWrapper} >
				<Button
					title="Click me!"
					onPress={onButtonClick} />
			</View>

			<View style={styles.buttonWrapper} >
				<CustomButton
					title="custom - click me!"
					onPress={onButtonClick} />
			</View>

		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 15
	},
	title: {
		fontSize: 30,
		fontWeight: 'bold'
	},
	buttonWrapper: {
		alignSelf: "stretch",
		paddingTop: 50
	}
});
