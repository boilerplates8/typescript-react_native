
import React, {FunctionComponent} from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';


interface CustomButtonProps {
	title: string;
	onPress(): void;
}

export const CustomButton: FunctionComponent<CustomButtonProps> = (props) => {
    
    const {title, onPress} = props;

	return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.button}
            onPress={onPress} >

			<Text style={styles.text} >
                {title}
            </Text>

		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
    button: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: "#999",
        borderRadius: 8,
        elevation: 5
    },
    text: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        textTransform: 'capitalize'
    }
});
